# Search app

1.  Be able to attach to multiple input by js.
2.  Suggest layer should be displayed when input is focused, and hidden when areas outside are clicked.
3.  Show all the matching items(if no input, show all the apps), with logo & name, at max height 400px.
4.  Clicking item(or pressing ENTER key when item is focused) should update the input field with corresponding app name.
5.  Support both keyboard & mouse selecting for PC browsers, and there must be no conflict when using both. 