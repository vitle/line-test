var selectedIndex = 0
var dataTmp = []

// when use have even onchange input
onChange = () => {
    setTimeout(()=>{
        let key = document.getElementsByName("apps")[0].value
        let data = searchData(key)
        let htmldata = generateData(data)
        document.getElementById("suggests").innerHTML = htmldata
        selectedItem(0)
    },200)
}

// filter data
searchData = (key) =>{
    return TABLE_DATA.filter( item =>{
        if(item.name.toLowerCase().includes(key.toLowerCase()))
            return item
    })
}

// create list suggest
generateData = (data) =>{
    dataTmp = data
    let arrHtml =  data.map((item,index) => {
        return generateItem(item,index)
    })
    let listItem = `<div class="list-group div-scroll" tabindex="0">
                            ${arrHtml.join("")}
                    </div>`
    return listItem
}

// create item suggest
generateItem = (item,index) =>{
    return `<button class="item list-item list-group-item-action" 
                    key="${index}" 
                    onmouseover='focusItem(${index})' 
            >
                <img src="${item.thumbnailUrl}" /> 
                <span>
                    ${encodeString(item.name)} 
                </span>
            </button>`
}

// encode string for XSS
encodeString = (value) =>{
    return value.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
        return '&#'+i.charCodeAt(0)+';';
     });
}


// event listen click list suggest
document.getElementById("suggests").addEventListener("click", function(e) {
    addItem()
})

// event listen in input search
document.getElementById("search").addEventListener("keyup",function(e){
    if(event.keyCode == 13)
        addItem()
    else
        onChange()
})

// listen event focus in input search
document.getElementById("search").addEventListener("focus", function(){
    onChange()
    document.getElementById("suggests").style.display = "block"
    selectedItem(0)
});

//listen event blur in input search
document.getElementById("search").addEventListener("blur", function(){
    setTimeout(()=>{
        document.getElementById("suggests").innerHTML = ""
        document.getElementById("suggests").style.display = "none"
        selectedIndex = 0
    },200)
});

// add value to input and logs
addItem = () =>{
    if(!dataTmp[selectedIndex])
        return
    let value = dataTmp[selectedIndex].name
    showSearchData(dataTmp[selectedIndex])
    document.getElementById("search").value = value
    document.getElementById("search").blur()
    selectedIndex = 0
    logHistory(value)
}

showSearchData = (data) =>{
    document.getElementById("app-name").innerHTML = `
        <div class="margin-app">
            <div class="col-md-12">
                <h4>Result: </h4>
                <hr/>
            </div>
            <div class="col-md-4 col-xs-4">
                <img src="${data.thumbnailUrl}" class="img-responsive" />
            </div>
            <div class="col-md-8" style="overflow: hidden;"><h3>${encodeString(data.name)}</h3></div>
        </div>
    `
}

// change color when hover or seleted it
selectedItem = (index) =>{
    if(!document.querySelectorAll('[key]')[selectedIndex] || !document.querySelectorAll('[key]')[index])
        return
    document.querySelectorAll('[key]')[selectedIndex].style.backgroundColor = "#ffffff"
    selectedIndex = index
    document.querySelectorAll('[key]')[selectedIndex].style.backgroundColor = "#e6f7ff"
}

// clicked to item in list suggest
focusItem = (index) =>{
    selectedItem(index)
}

// focus to input
focusSearch = () =>{
    onChange()
    document.getElementById("suggests").style.display = "block"
    selectedItem(0)
}

// blur to input
blurSearch = () =>{
    setTimeout(()=>{
        document.getElementById("suggests").innerHTML = ""
        document.getElementById("suggests").style.display = "none"
        selectedIndex = 0
    },200)
}

// log value to local storage
logHistory = (value) =>{
    let data = localStorage.getItem("logs")? JSON.parse(localStorage.getItem("logs")):[]
    let check = data.find(item =>{
        return value == item
    })
    if(!check)
        data.push(value)
    localStorage.setItem("logs",JSON.stringify(data))
}


