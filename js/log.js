// remove value to local storage
removeHistory = (index) =>{
    let data = JSON.parse(localStorage.getItem("logs"))
    data.splice(index, 1);
    localStorage.setItem("logs",JSON.stringify(data))
    generateLogs()
}

// generate logs
generateLogs = () =>{
    let data = JSON.parse(localStorage.getItem("logs"))
    let listItem = data.map((value, index)=>{
        return generateLogItem(value,index)
    })
    document.getElementById("logs").innerHTML = `
        <ul class="list-group">
            ${listItem.join("")}
        </ul>
    `
}

// genrate item log
generateLogItem = (value,index) =>{
    return `
                <li class="list-group-item">
                    ${encodeString(value)}   
                    <button type="button" class="close" aria-label="Close" onclick="removeHistory(${index})">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </li>
            `
}

// encode string for XSS
encodeString = (value) =>{
    return value.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
        return '&#'+i.charCodeAt(0)+';';
     });
}

generateLogs()